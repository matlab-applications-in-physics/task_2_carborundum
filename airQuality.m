%defining norms for five different substances
substances = ["SO2", "NO2", "O3", "CO", "PM10"];
norm_values = [350, 200, 120, 1000, 10];
%creating a dictionary with substance's names as keys
norms = containers.Map(substances, norm_values);

%changing commas to dots in .csv files
for i = 2:15
    if i <= 9
        filename = "air_quality_data_Zabrze\dane-pomiarowe_2020-11-0" + i + ".csv";
    else 
        filename = "air_quality_data_Zabrze\dane-pomiarowe_2020-11-" + i + ".csv";
    end
    
    %saving corrected data to new files (with names easier to iterate over)
    newFilename = "data-2020-11-" + i + ".csv";
    data = fileread(filename);
    data = strrep(data, ',','.');
    fileID = fopen(newFilename, 'w');
    fwrite(fileID, data, 'char');
    fclose(fileID);
end


resultFilename = "PMresults.dat";

%cleaning up PMresults.dat
writematrix([], resultFilename);

daysWeekly = 0;

for i = 2:15
    %reading air quality data of particular day
    filename = "data-2020-11-" + i + ".csv";
    data = readtable(filename, 'ReadVariableNames', false);
    
    isNormExceededDaily = 0;
    hoursDaily = 0;
    
    %checking if norm of any substances was exceeded in every hour of a day
    %if yes -- norm was exceeded this day, so isNormExceededDaily and 
    %isNormExceededHourly are set to 1 and we're incrementing hoursDaily
    for j = 1:24
        isNormExceededHourly = 0;
        
        if table2array(data(j, 2)) > norms("SO2")
            infoString = "2020-11-" + i + " " + table2array(data(j, 1)) + " SO2 " + table2array(data(j, 2));
            disp(infoString);
            writematrix(infoString, resultFilename, 'WriteMode', 'append');
            isNormExceededHourly = 1;
        end
        
        if table2array(data(j, 3)) > norms("NO2")
            infoString = "2020-11-" + i + " " + table2array(data(j, 1)) + " NO2 " + table2array(data(j, 3));
            disp(infoString);
            writematrix(infoString, resultFilename, 'WriteMode', 'append');
        end
        
        if table2array(data(j, 7)) > norms("O3")
            infoString = "2020-11-" + i + " " + table2array(data(j, 1)) + " O3 " + table2array(data(j, 7));
            disp(infoString);
            writematrix(infoString, resultFilename, 'WriteMode', 'append');
            isNormExceededHourly = 1;
        end
        
        if table2array(data(j, 9)) > norms("CO")
            infoString = "2020-11-" + i + " " + table2array(data(j, 1)) + " CO " + table2array(data(j, 9));
            disp(infoString);
            writematrix(infoString, resultFilename, 'WriteMode', 'append');
            isNormExceededHourly = 1;
        end
        
        if table2array(data(j, 10)) > norms("PM10")
            infoString = "2020-11-" + i + " " + table2array(data(j, 1)) + " PM10 " + table2array(data(j, 10));
            disp(infoString);
            writematrix(infoString, resultFilename, 'WriteMode', 'append');
            isNormExceededHourly = 1;
        end
        
        if isNormExceededHourly
            isNormExceededDaily = 1;
            hoursDaily = hoursDaily + 1;
        end
    end
    %display and write data about hours that norms were exceeded to file
    normExceededDailyInfoString = "hours_of_exceeded_norm " + hoursDaily;
    disp("2020-11-" + i + " norms were exceeded for " + hoursDaily + " hours.");
    writematrix(normExceededDailyInfoString, resultFilename, 'WriteMode', 'append');
    
    %increment daysWeekly if norm was exceeded that day
    if isNormExceededDaily
        daysWeekly = daysWeekly + 1;
    end
    
    %clear daysWeekly, display and save data about number of days that
    %norms were exceeded if the week has passed
    if mod((i - 1), 7) == 0
        normExceededWeeklyInfoString = "days_of_exceeded_norm " + daysWeekly;
        writematrix(normExceededWeeklyInfoString, resultFilename, 'WriteMode', 'append');
        disp("2020-11-(" + (i - 6) + "-" + i + ") norms were exceeded for " + daysWeekly + " days.");
        daysWeekly = 0;
    end
end